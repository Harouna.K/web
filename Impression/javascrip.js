document.getElementById('loginForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const email = document.getElementById('loginEmail').value;
    const password = document.getElementById('loginPassword').value;

    const validEmail = 'user@example.com';
    const validPassword = 'password123';

    // Simuler une vérification de connexion
    if (email === validEmail && password === validPassword) {
        // Connexion réussie
        window.location.href = 'commande.html'; // Redirection vers la page de service d'impression 3D
    } else {
        // Afficher un message d'erreur si les informations de connexion sont incorrectes
        document.getElementById('errorMessage').style.display = 'block';
    }
});
