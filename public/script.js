document.addEventListener('DOMContentLoaded', () => {
    const contactForm = document.getElementById('contactForm');
    const responseMessage = document.getElementById('responseMessage');

    contactForm.addEventListener('submit', async (event) => {
        event.preventDefault();

        // Récupérer les valeurs des champs du formulaire
        const formData = {
            firstName: document.getElementById('firstName').value.trim(),
            lastName: document.getElementById('lastName').value.trim(),
            country: document.getElementById('country').value.trim(),
            city: document.getElementById('city').value.trim(),
            number: document.getElementById('number').value.trim(),
            email: document.getElementById('email').value.trim(),
            message: document.getElementById('message').value.trim(),
        };

        // Validation des champs obligatoires
        if (
            formData.firstName === '' || formData.lastName === '' ||
            formData.country === '' || formData.city === '' ||
            formData.number === '' || formData.email === '' || formData.message === ''
        ) {
            responseMessage.textContent = 'Veuillez remplir tous les champs du formulaire.';
            return;
        }

        // Validation de l'adresse email
        const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!emailPattern.test(formData.email)) {
            responseMessage.textContent = 'Veuillez saisir une adresse email valide.';
            return;
        }

        try {
            // Envoi des données du formulaire au serveur
            const response = await fetch('/contact', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formData),
            });

            if (!response.ok) {
                throw new Error('Une erreur est survenue lors de l\'envoi du message.');
            }

            // Réinitialisation du formulaire et affichage du message de succès
            const result = await response.text();
            responseMessage.textContent = 'Votre message a été envoyé avec succès.';
            contactForm.reset();
        } catch (error) {
            // Affichage du message d'erreur en cas d'échec de l'envoi
            responseMessage.textContent = error.message;
        }
    });
});
