const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const PORT = process.env.PORT || 3000;

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../public')));

// Routes
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../index.html'));
});

app.post('/contact', (req, res) => {
    const { firstName, lastName, country, city, number, email, message } = req.body;

    // Validation des données
    if (!firstName || !lastName || !country || !city || !number || !email || !message) {
        console.error('Validation échouée : tous les champs sont requis.');
        return res.status(400).send('Tous les champs sont requis.');
    }

    const contactMessage = { firstName, lastName, country, city, number, email, message, date: new Date() };
    console.log('Message de contact reçu:', contactMessage);

    const filePath = path.join(__dirname, 'contactMessages.json');
    console.log('Chemin absolu de contactMessages.json:', filePath);

    fs.readFile(filePath, (err, data) => {
        if (err && err.code !== 'ENOENT') {
            console.error('Erreur lors de la lecture du fichier:', err);
            return res.status(500).send('Erreur lors de la lecture des messages.');
        }

        const messages = data ? JSON.parse(data) : [];
        console.log('Messages existants:', messages);

        messages.push(contactMessage);
        console.log('Nouveaux messages:', messages);

        fs.writeFile(filePath, JSON.stringify(messages, null, 2), (err) => {
            if (err) {
                console.error('Erreur lors de l\'écriture du fichier:', err);
                return res.status(500).send('Erreur lors de la sauvegarde du message.');
            }
            console.log('Message enregistré avec succès.');
            res.send('Nous avons bien reçu votre demande. Merci!');
        });
    });
});

app.listen(PORT, () => {
    console.log(`Serveur en écoute sur le port ${PORT}`);
});
